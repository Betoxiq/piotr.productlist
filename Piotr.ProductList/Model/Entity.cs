using System;

namespace Piotr.ProductList.Model
{
    public abstract class Entity
    {
        public Guid Id { get; set; }
    }
}